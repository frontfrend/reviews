const Bundler = require("parcel-bundler");
const express = require("express");
const proxy = require("http-proxy-middleware");

const app = express();
const bundler = new Bundler("index.html", {});

const apiProxy = proxy("/api", {
  target: "http://localhost:3000",
  changeOrigin: true
});

app.use(bundler.middleware());
app.use(apiProxy);

app.listen(1234);

if (module.hot) {
  module.hot.accept();
}

import { observable, action } from "mobx";
import { getUserReviews, getUser } from "../api/user";
import cookies from "browser-cookies";

class UserStore {
  @observable user = null;
  @observable reviews = [];

  constructor() {
    this.fetchUser();
  }

  @action
  setUser = user => {
    this.user = user;
  };
  @action
  setReviews = user => {
    this.reviews = user;
  };

  fetchUser = async () => {
    const userResponse = await getUser();

    if (userResponse.status === 200) {
      const user = await userResponse.json();
      this.setUser(user);
    }
  };

  fetchReviews = async () => {
    const reviewsResponse = await getUserReviews();

    if (reviewsResponse.status === 200) {
      const reviews = await reviewsResponse.json();
      this.setReviews(reviews || []);
    }
  };

  @action
  forget = () => {
    this.user = null;
    cookies.erase("token");
  };
}

export default new UserStore();

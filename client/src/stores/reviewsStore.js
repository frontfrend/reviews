import { observable, action, reaction } from "mobx";
import {
  deleteReview,
  editReview,
  addReview,
} from "../api/review";
import {getReviewsByHouse} from "../api/house";
import user from "./userStore";
import houses from "./housesStore";

class ReviewsStore {
  @observable reviews = [];
  @observable houseReviews = [];
 
  @action
  setHouses = reviews => {
    this.reviews = reviews || [];
  };

  @action
  setHouseReviews = houseReviews => {
    this.houseReviews = houseReviews || [];
  };

  @action
  setZoom = event => {
    this.zoom = event.target._zoom;
    console.log(this.zoom);
  };

  @action
  setCenter = () => {
    this.center = this.map.getCenter();
    console.log(this.center);
  };

  @action
  deleteReview = (id, houseId) => {
    deleteReview(id, houseId).then(() => {
      houses.getHouses();
      user.fetchReviews();
    });
  };

  @action
  editReview = data => {
    editReview(data).then(() => {
      houses.getHouses();
      user.fetchReviews();
    });
  };

  @action
  addReview = data => {
    addReview(data).then(() => {
      houses.getHouses();
      user.fetchReviews();
    });
  };

  fetchDetailReviews = async houseId => {
    this.setHouseReviews([]);

    const reviewsResponse = await getReviewsByHouse(houseId);
    if (reviewsResponse.status === 200) {
      const reviews = await reviewsResponse.json();
      this.setHouseReviews(reviews);
    }
  };
}

const reviewsStore = new ReviewsStore();

reaction(
  () => reviewsStore.reviews,
  () => {
    reviewsStore.refreshMarkers();
  }
);

export default reviewsStore;

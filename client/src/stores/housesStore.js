import { observable, action, reaction } from "mobx";
import { getHouses } from "../api/house";
import MarkerClusterer from "../utils/markerclusterer";
import history from "../history";

class HousesStore {
  @observable houses = [];
  @observable map = null;
  @observable markers = null;
  @observable houseReviews = [];
  zoom = 11;
  center = { lat: 55.759191, lng: 37.619248 };

  constructor() {
    this.getHouses();
  }
  @action
  setHouses = houses => {
    this.houses = houses || [];
  };

  @action
  setHouseReviews = houseReviews => {
    this.houseReviews = houseReviews || [];
  };

  @action
  setZoom = () => {
    this.zoom = this.map.getZoom();
  };

  @action
  setCenter = () => {
    this.center = this.map.getCenter();
  };

  initMap = () => {
    if (this.map) {
      window.google.maps.event.removeListener(this.dragedListener);
      window.google.maps.event.removeListener(this.zoomListener);
    }
    this.map = new window.google.maps.Map(document.getElementById("map"), {
      center: this.center,
      zoom: this.zoom
    });

    this.dragedListener = this.map.addListener(
      "center_changed",
      this.setCenter
    );
    this.zoomListener = this.map.addListener("zoom_changed", this.setZoom);

    this.refreshMarkers();
  };

  refreshMarkers = () => {
    if (!this.map) {
      return;
    }
    if (this.markerCluster) {
      this.markerCluster.clearMarkers();
    }
    this.markers = this.houses.map(function(house) {
      const label = house.rate;

      const marker = new window.google.maps.Marker({
        position: house.location,
        label,
        house
      });
      marker.addListener("click", function() {
        history.push(`/house/${this.house.id}`);
      });

      return marker;
    });

    // Add a marker clusterer to manage the markers.
    this.markerCluster = new MarkerClusterer(this.map, this.markers, {
      imagePath:
        "https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m"
    });
  };

  getHouses = async () => {
    const housesResponse = await getHouses();
    if (housesResponse.status === 200) {
      const houses = await housesResponse.json();
      this.setHouses(houses);
    }
  };
}

const housesStore = new HousesStore();
global.initMap = housesStore.initMap;
reaction(
  () => housesStore.houses,
  () => {
    housesStore.refreshMarkers();
  }
);

export default housesStore;

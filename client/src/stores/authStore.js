import { observable, action } from 'mobx'; 
 

class AuthStore {
  @observable isAuth = null;
 

  @action logout() {
     
  }
}

export default new AuthStore();
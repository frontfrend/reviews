import { createBrowserHistory } from 'history'

export default createBrowserHistory({
    forceRefresh: false
  /* pass a configuration object here if needed */
})
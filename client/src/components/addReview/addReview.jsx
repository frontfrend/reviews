import React from "react";
import Modal from "antd/es/modal/Modal";
import InputNumber from "antd/es/input-number/index";
import Input from "antd/es/input/index";
import Form from "antd/es/form/index";
import Rate from "antd/es/rate/index";
import { Autosuggest } from "../autosuggest";
import { observer, inject } from "mobx-react";

const desc = ["Очень плохо", "Плохо", "В целом хорошо", "Хорошо", "Квартира мечты"];

@inject("reviews")
class AddReviewModal extends React.Component {
  saveHandler = () => {
    const { edit, form, initFields, reviews } = this.props;
    form.validateFields((err, values) => {
      if (!err) {
        const { address, ...rest } = values;
        if (!edit) {
          const data = {
            ...rest,
            description: address.address.description,
            placeId: address.address.place_id
          };
          reviews.addReview(data);
        } else {
          reviews.editReview({ ...rest, id: initFields.id });
        }

        this.props.close();
      }
    });
  };
  render() {
    const { visible, close, initFields = {}, title, form, edit } = this.props;
    const { getFieldDecorator, getFieldValue } = form;
    const rateValue = getFieldValue("rate");

    return (
      <Modal
        title={title}
        visible={visible}
        onOk={this.saveHandler}
        onCancel={close}
        okText="Сохранить"
        cancelText="Отмена"
      >
        <Form>
          {!edit ? (
            <Form.Item>
              {getFieldDecorator("address", {
                rules: [{ required: true, message: "Введите адрес" }],
                initialValue: initFields.description
              })(<Autosuggest placeholder="Введите адрес" />)}
            </Form.Item>
          ) : (
            <Form.Item>
              <Input value={initFields.description} disabled />
            </Form.Item>
          )}

          <Form.Item>
            {getFieldDecorator("apartment", {
              initialValue: initFields.apartment,
              rules: [{ required: true, message: "Введите номер квартиры" }]
            })(
              <InputNumber
                style={{ width: "140px" }}
                min={0}
                max={1000}
                step={1}
                placeholder="Номер квартиры"
              />
            )}
          </Form.Item>

          <Form.Item>
            <span>
              {getFieldDecorator("rate", {
                initialValue: initFields.rate,
                rules: [{ required: true, message: "Оцените квартиру" }]
              })(<Rate tooltips={desc} />)}

              {rateValue ? (
                <span className="ant-rate-text">{desc[rateValue - 1]}</span>
              ) : (
                ""
              )}
            </span>
          </Form.Item>

          <Form.Item>
            {getFieldDecorator("review", {
              initialValue: initFields.review,
              rules: [{ required: true, message: "Напишите отзыв" }]
            })(
              <Input.TextArea
                rows={4}
                placeholder="Что вам не понравилось или понравилось в этой квартире?"
                maxLength="1000"
              />
            )}
          </Form.Item>
        </Form>
      </Modal>
    );
  }
}

export default Form.create({ name: "normal_login" })(observer(AddReviewModal));

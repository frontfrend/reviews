import React from "react";
import Component from "./addReview";
import { observable, computed, action } from "mobx";
import { MobXProviderContext } from "mobx-react";

class ReviewModel {
  @observable apartment = null;
  @observable address = null;
  @computed
  get getAapartment() {
    return this.apartment;
  }

  @action
  changeAapartment = apartment => {
    this.apartment = apartment;
  };

  @action
  changeAddress = address => {
    this.address = address;
  };
}

const store = new ReviewModel();

export const AddReviewModal = ({ edit, ...props }) => {
  const { user } = React.useContext(MobXProviderContext);

  const editable = user.reviews.find(item => item.id === edit);
  const additionalProps = {
    title: "Добавить отзыв"
  };

  if (editable) {
    additionalProps.title = "Редактировать отзыв";
  }
  return (
    <Component
      {...props}
      initFields={editable}
      edit={!!editable}
      store={store}
      {...additionalProps}
    />
  );
};

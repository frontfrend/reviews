import React from "react";
import Select from "antd/es/select/index";
import Spin from "antd/es/spin/index";
import { fetchSuggestion } from "../../api/address";

const Option = Select.Option;

export class Autosuggest extends React.Component {
  state = {
    data: [],
    fetching: false
  };
  timer = null;

  fetcher = value => () => {
    fetchSuggestion(value)
      .then(response => response.clone().json())
      .then(body => {
        const data = body.predictions.filter(address => address.id).map(address => ({
          text: address.description,
          value: address.id,
          address: address
        }));
        this.setState({ data, fetching: false });
      });
  };
  fetchUser = value => {
    this.setState({ data: [], fetching: true });
    clearTimeout(this.timer);
    this.timer = setTimeout(this.fetcher(value), 1500);
  };

  handleChange = value => {
    this.props.onChange({
      ...this.state.data.find(item => item.value === value.key),
      ...value
    });
    this.setState({
      value,
      data: [],
      fetching: false
    });
  };
  render() {
    const { value, onChange: _onChange, ...rest } = this.props;
    const { fetching, data } = this.state;
    return (
      <Select
        labelInValue
        showSearch
        notFoundContent={fetching ? <Spin size="small" /> : null}
        filterOption={false}
        onSearch={this.fetchUser}
        onChange={this.handleChange}
        value={value}
        {...rest}
      >
        {data.map(d => (
          <Option key={d.value}>{d.text}</Option>
        ))}
      </Select>
    );
  }
}

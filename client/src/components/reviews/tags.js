import styled from "@emotion/styled";
import Button from "antd/es/button";

export const Actions = styled(Button)`
  margin: 0 10px;

  &:first-of-type {
    margin-left: auto;
  }

  @media (min-width: 320px) and (max-width: 767px) {
    margin: 0 0 5px;
  }
`;

export const Text = styled.span``;
export const Container = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
`;

export const ActionsContainer = styled.div`
  display: flex;
  justify-content: space-between;

  @media (min-width: 320px) and (max-width: 767px) {
    flex-direction: column;
  }
`;

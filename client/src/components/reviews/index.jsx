// @ts-check
import React from "react";
import Collapse from "antd/es/collapse";
import { Header } from "./header";

class Reviews extends React.Component {
  render() {
    const { reviews } = this.props;
    console.log(reviews.length);
    return (
      <div>
        <Collapse accordion>
          {reviews.map(({ id, description, rate, review, apartment, houseId }) => (
            <Collapse.Panel
              header={<Header id={id} houseId={houseId} text={description} />}
              key={id}
            >
              <p>Оценка: {rate}/5</p>
              <p>Отзыв: {review}</p>
              <p>Квартира: {apartment}</p>
            </Collapse.Panel>
          ))}
        </Collapse>
      </div>
    );
  }
}

export default Reviews;

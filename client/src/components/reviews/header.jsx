import React, { useState } from "react";
import { Actions, ActionsContainer, Text, Container } from "./tags";
import { AddReviewModal } from "../addReview";
 import { inject, observer } from "mobx-react";
 
const Header = inject('user', 'reviews')(observer(({ text, id, reviews, houseId }) => {
  const [visible, changeVisibility] = useState();
  const edit = () => {
    changeVisibility(true);
  };
  return (
    <Container>
      <Text>{text}</Text>
      <ActionsContainer>
        <>
          <Actions onClick={edit}>Редактировать</Actions>
          <Actions onClick={() => reviews.deleteReview(id, houseId)} type="danger">
            Удалить
          </Actions>
        </>
      </ActionsContainer>
      <AddReviewModal
        edit={id}
        close={() => changeVisibility(false)}
        visible={visible}
      />
    </Container>
  );
}));

export  {
  Header
}

import React from "react";
import GM from "@google/maps";
import styled from "@emotion/styled";
import { inject } from "mobx-react";
console.log(GM);
const MapContainer = styled.div`
  width: 100%;
  height: 100%;
`;

@inject("houses")
class Map extends React.Component {
  componentDidMount() {
    this.props.houses.initMap();
  }

  render() {
    return <MapContainer id="map" />;
  }
}

export default Map;

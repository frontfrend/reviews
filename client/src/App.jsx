import { hot } from "react-hot-loader/root";
import React, { Suspense } from "react";
import { Global } from "@emotion/core";
import cssReset from "./utils/cssReset";
import routerStore from "./stores/routerStore";
import authStore from "./stores/authStore";
import userStore from "./stores/userStore";
import reviewsStore from "./stores/reviewsStore";
import housesStore from "./stores/housesStore";
import "antd/dist/antd.css";
import { Provider } from "mobx-react";
import history from "./history";

import styled from "@emotion/styled";
import { Router, Route } from "react-router-dom";

const Root = styled.div`
  height: 100%;
  > .ant-layout {
    height: 100%;
  }
`;

const stores = {
  router: routerStore,
  auth: authStore,
  user: userStore,
  reviews: reviewsStore,
  houses: housesStore
};

const MainLayout = React.lazy(() => import("./pages/main"));
const Login = React.lazy(() => import("./pages/login"));
const Register = React.lazy(() => import("./pages/register"));
const UsersPage = React.lazy(() => import("./pages/user"));
const HousePage = React.lazy(() => import("./pages/house"));

const App = () => (
  <Provider {...stores}>
    <Router history={history}>
      <Root>
        <Global styles={cssReset} />
        <Route
          path="/"
          exact
          component={props => (
            <Suspense fallback={<div />}>
              <MainLayout {...props} />
            </Suspense>
          )}
        />
        <Route
          path="/login"
          exact
          component={props => (
            <Suspense fallback={<div />}>
              <Login {...props} />
            </Suspense>
          )}
        />
        <Route
          path="/profile"
          exact
          component={props => (
            <Suspense fallback={<div>Loading</div>}>
              <UsersPage {...props} />
            </Suspense>
          )}
        />
        <Route
          path="/register"
          exact
          component={props => (
            <Suspense fallback={<div />}>
              <Register {...props} />
            </Suspense>
          )}
        />
        <Route
          path="/house/:id"
          exact
          component={props => (
            <Suspense fallback={<div />}>
              <HousePage {...props} />
            </Suspense>
          )}
        />
      </Root>
    </Router>
  </Provider>
);

export default hot(App);

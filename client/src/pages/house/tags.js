import styled from "@emotion/styled"; 
import Select from "antd/es/select";
import List from "antd/es/list";

export const Container = styled.div`
  width: 300px;
  margin: 0 auto;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-content: center;
  align-items: center;
`;
 
export const SelectApartment = styled(Select)`
  width: 250px;
  margin-bottom: 20px;
`;

export const LItem = styled(List.Item)`
 
`;
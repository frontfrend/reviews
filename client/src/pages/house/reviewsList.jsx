import React from "react";
import List from "antd/es/list";
import { LItem } from "./tags";

export const ReviewList = ({ reviews }) => {
    return <List
    bordered
    dataSource={reviews}
    renderItem={review => (
      <LItem>
          <List.Item.Meta 
          title={`Оценка: ${review.rate}/5`}
          description={review.review}
        /> 
      </LItem>
    )}
  />
  
};

import React from "react";
import PageHeader from "antd/es/page-header";
import Layout from "antd/es/layout";
import { inject, observer } from "mobx-react";
import Select from "antd/es/select";
import Empty from "antd/es/empty";
import { ReviewList } from "./reviewsList";
import { SelectApartment, Container } from "./tags";

@inject("reviews")
@observer
class HousePage extends React.Component {
  state = {
    apartment: undefined,
    apartmentReviews: null
  };

  back = () => {
    this.props.history.goBack();
  };

  componentDidMount() {
    const { match } = this.props;
    this.props.reviews.fetchDetailReviews(match.params.id);
  }

  onSelectApartment = value => {
    const { reviews } = this.props;

    this.setState({
      apartment: value,
      apartmentReviews: reviews.houseReviews.filter(r => r.apartment === value)
    });
  };

  render() {
    const { reviews } = this.props;
    const { apartment, apartmentReviews } = this.state;

    const options = reviews.houseReviews.map(review => (
      <Select.Option value={review.apartment} key={review.apartment}>
        Квартира {review.apartment}
      </Select.Option>
    ));

    return (
      <div>
        <PageHeader
          onBack={this.back}
          title={(reviews.houseReviews[0] || {}).description}
        />
        <Layout.Content>
          <Container>
            <SelectApartment
              onChange={this.onSelectApartment}
              value={apartment}
              placeholder="Выберите номер квартиры"
            >
              {options}
            </SelectApartment>
            {apartmentReviews ? (
              <ReviewList reviews={apartmentReviews} />
            ) : (
              <Empty
                image={Empty.PRESENTED_IMAGE_SIMPLE}
                description="Данных нет"
              />
            )}
          </Container>
        </Layout.Content>
      </div>
    );
  }
}

export default HousePage;

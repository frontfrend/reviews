import styled from "@emotion/styled";
import Layout from "antd/es/layout/index";
import Button from "antd/es/button";
import media from "../../media";

export const Header = styled(Layout.Header)`
  display: flex;
  justify-content: space-between;
  align-items: center;
  background: white;

  ${media.mobile`
      padding: 0 8px;
  `}
`;
export const Content = styled(Layout.Content)`
  height: 100%;
`;

export const HeaderButton = styled(Button)`
  margin-left: 15px; 
`;

export const ExitButton = styled(Button)`
  margin-left: 15px;
  margin-left: auto;
  min-width: 32px;

`;

export const MyReviewBtn = styled(Button)`
  margin-left: 15px;
  margin-right: 15px;
`;

export const UnauthUserContainer = styled.div`
  display: flex;
  justify-content: flex-end;
  width: 100%;
`;

export const AuthUserContainer = styled.div`
  display: flex;
  width: 100%;
`;

export const UserActions = styled.div`
  display: flex;
`;

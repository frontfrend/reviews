import React from "react";
import Layout from "antd/es/layout/index";
import Map from "../../components/map/map";
import { Header, Content } from "./tags";
import { withRouter } from "react-router-dom";
import UserBlock from "./user";

class MainLayout extends React.PureComponent {
  onChange = () => {};

  render() {
    return (
      <Layout>
        <Header>
          <UserBlock {...this.props} />
        </Header>
        <Content>
          <Map />
        </Content>
      </Layout>
    );
  }
}
export default withRouter(MainLayout);

import React from "react";
import {
  HeaderButton,
  UnauthUserContainer,
  MyReviewBtn,
  AuthUserContainer,
  UserActions,
  ExitButton
} from "./tags";
import { inject, observer } from "mobx-react";
import Button from "antd/es/button";
import { AddReviewModal } from "../../components/addReview";

@inject("user")
@observer
class UserBlock extends React.PureComponent {
  state = {
    visible: false
  };
  goToLogin = () => {
    this.props.history.push("/login");
  };

  goToProfile = () => {
    this.props.history.push("/profile");
  };
  goToRegister = () => {
    this.props.history.push("/register");
  };
  showModal = () => {
    this.setState({ visible: true });
  };
  closeModal = () => {
    this.setState({ visible: false });
  };

  render() {
    if (this.props.user.user) {
      return (
        <AuthUserContainer>
          <UserActions>
            <Button onClick={this.showModal}>Добавить отзыв</Button>
            <MyReviewBtn onClick={this.goToProfile}>Мои отзывы</MyReviewBtn>
          </UserActions>

          <ExitButton shape="circle" icon="poweroff" onClick={this.props.user.forget}></ExitButton>
          <AddReviewModal
            close={this.closeModal}
            visible={this.state.visible}
          />
        </AuthUserContainer>
      );
    }
    return (
      <UnauthUserContainer>
        <HeaderButton onClick={this.goToLogin}>Вход</HeaderButton>
        <HeaderButton onClick={this.goToRegister}>Регистрация</HeaderButton>
      </UnauthUserContainer>
    );
  }
}

export default UserBlock;

import React from "react";
import Layout from "antd/es/layout/index";
import Input from "antd/es/input/index";
import Form from "antd/es/form/index";
import Spin from "antd/es/spin";
import { CustomForm, SubmitBtn, CustomHeader } from "./tags";
import { loginUser } from "../../api/user";
import { inject } from "mobx-react";

@inject("user")
class Login extends React.Component {
  state = {
    fetch: false,
    dataSource: []
  };

  handleChange = value => {
    this.setState({
      dataSource:
        !value || value.indexOf("@") >= 0
          ? []
          : [`${value}@gmail.com`, `${value}@mail.ru`, `${value}@yahoo.com`]
    });
  };

  back = () => {
    this.props.history.goBack();
  };

  onSubmit = e => {
    const { fetch } = this.state;
    e.preventDefault();

    if(fetch) {
      return;
    }
    this.setState({ fetch: true });

    this.props.form.validateFields((err, values) => {
      if (!err) {
        loginUser(values)
          .then(response => {
            if (response.status === 200) {
              this.props.user.fetchUser();
              this.back();
            } else {
              return response.json();
            }
          })
          .then(data => {
            if (data && data.message) {
              this.props.form.setFields({
                login: {
                  value: values.login,
                  errors: [new Error(data.message)]
                }
              });
            }
          })
          .finally(() => {
            this.setState({ fetch: false });
          });
      }
    });
  };

  render() {
    const { getFieldDecorator } = this.props.form;
    return (
      <div>
        <CustomHeader onBack={this.back} title="Создание нового пользователя" />
        <Layout.Content>
          <CustomForm onSubmit={this.onSubmit}>
            <Form.Item>
              {getFieldDecorator("login", {
                rules: [{ required: true, message: "Необходимое поле" }]
              })(<Input type="email" placeholder="Введите email" />)}
            </Form.Item>
            <Form.Item>
              {getFieldDecorator("password", {
                rules: [{ required: true, message: "Необходимое поле" }]
              })(<Input.Password placeholder="Введите пароль" />)}
            </Form.Item>
            <Spin spinning={this.state.fetch}>
              <SubmitBtn disabled={this.state.fetch} htmlType="submit">
                Вход
              </SubmitBtn>
            </Spin>
          </CustomForm>
        </Layout.Content>
      </div>
    );
  }
}

export default Form.create({ name: "login" })(Login);

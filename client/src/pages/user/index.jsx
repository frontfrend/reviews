import React from "react";
import PageHeader from "antd/es/page-header";
import Layout from "antd/es/layout";
import Reviews from "../../components/reviews";
import { inject, observer } from "mobx-react";

@inject("user")
@observer
class UsersPage extends React.Component {
  back = () => {
    this.props.history.goBack();
  };

  componentDidMount() {
    this.props.user.fetchReviews();
  }

  render() {
    return (
      <div>
        <PageHeader onBack={this.back} title="Мои отзывы" />
        <Layout.Content>
        <Reviews reviews={this.props.user.reviews} />
        </Layout.Content>
      </div>
    );
  }
}

export default UsersPage;

import styled from "@emotion/styled";
import Button from "antd/es/button";
import Form from "antd/es/form/index";

export const CustomForm = styled(Form)`
  width: 350px;
  margin: 50px auto;
  display: flex;
  flex-direction: column;
  justify-content: center;
`;

export const SubmitBtn = styled(Button)`
  margin: 0 auto;
`;

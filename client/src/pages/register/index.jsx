import React from "react";
import Layout from "antd/es/layout/index";
import Input from "antd/es/input/index";
import Spin from "antd/es/spin";
import Form from "antd/es/form/index";
import { CustomForm, SubmitBtn, CustomHeader } from "./tags";
import { createUser } from "../../api/user";
import { inject } from "mobx-react";

@inject("user")
class Register extends React.Component {
  state = {
    fetch: false
  };
  back = () => {
    this.props.history.goBack();
  };

  onSubmit = e => {
    const { fetch } = this.state;
    e.preventDefault();

    if(fetch) {
      return;
    }
    this.setState({ fetch: true });
    this.props.form.validateFields((err, values) => {
      if (!err) {
        createUser(values)
          .then(response => {
            if (response.status !== 201) {
              return response.json();
            } else {
              this.back();
              this.props.user.fetchUser();
            }
          })
          .then(data => {
            if (data && data.message) {
              this.props.form.setFields({
                login: {
                  value: values.login,
                  errors: [new Error(data.message)]
                }
              });
            }
          })
          .finally(() => {
            this.setState({ fetch: false });
          });
      }
    });
  };

  render() {
    const { getFieldDecorator } = this.props.form;
    return (
      <div>
        <CustomHeader onBack={this.back} title="Создание нового пользователя" />

        <Layout.Content>
          <CustomForm onSubmit={this.onSubmit}>
            <Form.Item>
              {getFieldDecorator("login", {
                rules: [{ required: true, message: "Необходимое поле" }]
              })(<Input type="email" placeholder="Введите email" />)}
            </Form.Item>
            <Form.Item>
              {getFieldDecorator("password", {
                rules: [{ required: true, message: "Необходимое поле" }]
              })(<Input.Password placeholder="Введите пароль" />)}
            </Form.Item>
            <Spin spinning={this.state.fetch}>
              <SubmitBtn disabled={this.state.fetch} htmlType="submit">Создать</SubmitBtn>
            </Spin>
            
          </CustomForm>
        </Layout.Content>
      </div>
    );
  }
}

export default Form.create({ name: "register" })(Register);

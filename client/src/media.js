import css from "@emotion/css";

const sizes = {
    mobile: 767,
    tablet: 1023,
 }

 export const media = Object.keys(sizes).reduce((acc, label) => {
    acc[label] = (...args) => css`
       @media (max-width: ${sizes[label]}px) {
          ${css(...args)};
       }
    `
    return acc
 }, {})

 export default media;
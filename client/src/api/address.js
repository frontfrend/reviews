import memoizee from "memoizee";

const AUTOSUGGEST_URL = `/api/addres/autosuggest?query=`;
export const fetchUnmemSuggestion = query => fetch(AUTOSUGGEST_URL + query);
export const fetchSuggestion = memoizee(fetchUnmemSuggestion, {
  promise: true
});

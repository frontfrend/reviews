const createReviewUrl = "/api/review/create";
const editReviewUrl = "/api/review/edit";
const deleteReviewsUrl = "/api/review/delete";

export const addReview = values => {
  values.houseId = values.placeId
  delete values.placeId
  return fetch(createReviewUrl, {
    method: "POST",
    headers: {
      "Content-Type": "application/json"
    },
    body: JSON.stringify(values)
  });
}

export const editReview = values =>
  fetch(editReviewUrl, {
    method: "PUT",
    headers: {
      "Content-Type": "application/json"
    },
    body: JSON.stringify(values)
  });

export const deleteReview = (id, houseId) =>
  fetch(deleteReviewsUrl, {
    method: "DELETE",
    headers: {
      "Content-Type": "application/json"
    },
    body: JSON.stringify({
      id,
      houseId
    })
  });

 
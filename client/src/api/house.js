const getHousesUrl = "/api/houses/houses";
const getHouseReviewsUrl = id => `/api/houses/house?houseId=${id}`;

export const getReviewsByHouse = houseId =>
  fetch(getHouseReviewsUrl(houseId), {
    method: "GET",
    headers: {
      "Content-Type": "application/json"
    }
  });

export const getHouses = () =>
  fetch(getHousesUrl, {
    method: "GET",
    headers: {
      "Content-Type": "application/json"
    }
  });

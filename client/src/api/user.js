const createUserUrl = "/api/user/create";
const loginUserUrl = "/api/user/login";
const reviewsUserUrl = "/api/user/reviews";
const getUserUrl = "/api/user";

export const createUser = values =>
  fetch(createUserUrl, {
    method: "POST",
    headers: {
      "Content-Type": "application/json"
    },
    body: JSON.stringify(values)
  });

export const loginUser = values =>
  fetch(loginUserUrl, {
    method: "POST",
    headers: {
      "Content-Type": "application/json"
    },
    credentials: "include",
    body: JSON.stringify(values)
  });

export const getUser = () =>
  fetch(getUserUrl, {
    method: "GET",
    headers: {
      "Content-Type": "application/json"
    },
    credentials: "include"
  });

export const getUserReviews = () =>
  fetch(reviewsUserUrl, {
    method: "GET",
    headers: {
      "Content-Type": "application/json"
    },
    credentials: "include"
  });

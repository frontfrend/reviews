import React from "react";
import { render } from "react-dom";
import { configure } from "mobx";

function renderApp() {
  const App = require("./App").default;
  render(<App />, document.getElementById("app"));
}

renderApp();

configure({
  enforceActions: "observed"
});

// module.hot.accept(renderApp);

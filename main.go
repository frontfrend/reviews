package main

import (
	"net/http"

	"gitlab.com/frontfrend/reviews/server/rest/google"
	"gitlab.com/frontfrend/reviews/server/rest/house"
	"gitlab.com/frontfrend/reviews/server/rest/review"
	"gitlab.com/frontfrend/reviews/server/rest/user"
)

func main() {
	google.HandleGoogle()
	review.HandleReview()
	user.HandleUser()
	house.HandleHouses()

	http.ListenAndServe(":3000", nil)
}

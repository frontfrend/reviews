#!/bin/bash
go build main.go

if [ -f "./main" ]
then
    echo "go build success"
    service review stop
    rm /var/www/rentreview.site/main
    mv ./main /var/www/rentreview.site/
    service review start
else
    echo "go build faild"
fi

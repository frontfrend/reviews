package review

import (
	"context"
	"encoding/json"
	"fmt"
	"log"
	"net/http"

	"gitlab.com/frontfrend/reviews/server/db"
	types "gitlab.com/frontfrend/reviews/server/rest"
	"gitlab.com/frontfrend/reviews/server/rest/google"
	"gitlab.com/frontfrend/reviews/server/rest/house"
	"gitlab.com/frontfrend/reviews/server/rest/private"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

func CreateReview(clientReview *types.ClientReview, userId primitive.ObjectID) {
	reviews := db.Client.Database("review").Collection("reviews")

	house := house.GetHouse(clientReview)
	review := types.Review{}
	review.HouseID = house.Id
	review.Apartment = clientReview.Apartment
	review.Rate = clientReview.Rate
	review.Review = clientReview.Review
	review.UserID = userId

	res, err := reviews.InsertOne(db.Ctx, review)
	if err != nil {
		fmt.Println("Error %v", err)
	}

	fmt.Println(res)
}

func EditReview(review *types.EditableReview, userId primitive.ObjectID) {
	collection := db.Client.Database("review").Collection("reviews")
	filter := bson.D{{"_id", review.ID}, {"userId", userId}}

	update := bson.D{
		{"$set", review},
	}
	_, err := collection.UpdateOne(db.Ctx, filter, update)

	if err != nil {
		fmt.Println("Error %v", err)
	}
}

func DeleteReview(review *types.DeleteReview, userId primitive.ObjectID) {
	reviews := db.Client.Database("review").Collection("reviews")
	filter := bson.D{{"_id", review.ID}, {"userId", userId}}
	_, err := reviews.DeleteOne(db.Ctx, filter)
	if err != nil {
		log.Println("Error %v", err)
	}
	searchReview := types.Review{}
	searchReviewError := reviews.FindOne(db.Ctx, bson.D{{"houseId", review.HouseID}}).Decode(&searchReview)
	log.Println("searchReviewError", searchReviewError)
	if searchReviewError != nil {
		log.Println("searchReviewError is not nil", review.HouseID)

		houses := db.Client.Database("review").Collection("houses")
		houses.DeleteOne(db.Ctx, bson.D{{"_id", review.HouseID}})
	}
}

func create(w http.ResponseWriter, r *http.Request) {
	clientUser := private.CheckJwtField(r)
	if clientUser == nil {
		w.WriteHeader(http.StatusForbidden)
		return
	}
	review := types.ClientReview{}
	decoder := json.NewDecoder(r.Body)
	decoder.DisallowUnknownFields()
	err := decoder.Decode(&review)
	if err != nil {
		panic(err)
	}
	place := google.GetPlace(review.HouseID)
	review.Location = place.Result.Geometry.Location
	CreateReview(&review, clientUser.ID)
}

func edit(w http.ResponseWriter, r *http.Request) {
	clientUser := private.CheckJwtField(r)
	if clientUser == nil {
		w.WriteHeader(http.StatusForbidden)
		return
	}
	review := types.EditableReview{}
	decoder := json.NewDecoder(r.Body)
	decoder.DisallowUnknownFields()
	err := decoder.Decode(&review)
	if err != nil {
		log.Println(err)
	}
	EditReview(&review, clientUser.ID)
}

func delete(w http.ResponseWriter, r *http.Request) {
	clientUser := private.CheckJwtField(r)
	if clientUser == nil {
		w.WriteHeader(http.StatusForbidden)
		return
	}
	review := types.DeleteReview{}
	decoder := json.NewDecoder(r.Body)
	decoder.DisallowUnknownFields()
	err := decoder.Decode(&review)
	if err != nil {
		log.Println(err)
		return
	}
	DeleteReview(&review, clientUser.ID)
	w.WriteHeader(http.StatusOK)
}

func getHouseReviews(w http.ResponseWriter, r *http.Request) {
	reviewsCollection := db.Client.Database("review").Collection("reviews")
	houseId := r.URL.Query().Get("houseId")
	var reviews []types.Review
	cur, err := reviewsCollection.Find(db.Ctx, bson.D{{"placeid", houseId}})
	if err != nil {
		panic(err)
	}

	for cur.Next(context.TODO()) {
		var review types.Review
		err := cur.Decode(&review)
		if err != nil {
			log.Fatal(err)
		}
		reviews = append(reviews, review)
	}
	b, _ := json.Marshal(reviews)
	w.WriteHeader(http.StatusOK)
	w.Write(b)
}

// HandleReview handle review's rest
func HandleReview() {

	http.HandleFunc("/api/review/delete", delete)
	http.HandleFunc("/api/review/edit", edit)
	http.HandleFunc("/api/review/create", create)
}

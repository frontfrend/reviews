package user

import (
	"context"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"time"

	"go.mongodb.org/mongo-driver/bson/primitive"

	types "gitlab.com/frontfrend/reviews/server/rest"
	"gitlab.com/frontfrend/reviews/server/rest/private"

	"gitlab.com/frontfrend/reviews/server/db"
	"go.mongodb.org/mongo-driver/bson"
)

func isUserExist(login string) bool {
	collection := db.Client.Database("review").Collection("users")
	existedUser := types.User{}

	findError := collection.FindOne(db.Ctx, bson.D{{"login", login}}).Decode(&existedUser)
	if findError != nil {
		return false
	}
	return true
}

func getUser(user *types.User) *types.User {
	collection := db.Client.Database("review").Collection("users")
	existedUser := types.User{}

	findError := collection.FindOne(db.Ctx, bson.D{
		{"login", user.Login}}).Decode(&existedUser)
	if findError != nil {
		return nil
	}
	if private.CheckPasswordHash(user.Password, existedUser.Password) {
		return &existedUser
	}
	return nil
}

func CreateUser(user *types.User) {
	user.CreatedAt = time.Now()
	collection := db.Client.Database("review").Collection("users")

	res, err := collection.InsertOne(db.Ctx, user)

	if err != nil {
		fmt.Println("Error %v", err)
	}

	fmt.Println(res)

}

// API START

func login(w http.ResponseWriter, r *http.Request) {
	user := types.User{}
	decoder := json.NewDecoder(r.Body)
	decoder.DisallowUnknownFields()
	err := decoder.Decode(&user)
	if err != nil {
		panic(err)
	}
	loginedUser := getUser(&user)

	if loginedUser == nil {
		w.WriteHeader(http.StatusNotFound)
		b, _ := json.Marshal(types.Error{"Пользователь с таким логином и паролем не существует"})
		w.Write(b)
		return
	}
	expiration := time.Now().Add(7 * 24 * time.Hour)
	token := private.GetJwtField(loginedUser.ID.Hex(), loginedUser.Password)

	cookie := http.Cookie{Name: "token", Value: token, Expires: expiration, Path: "/"}
	http.SetCookie(w, &cookie)
}

func create(w http.ResponseWriter, r *http.Request) {
	user := types.User{}
	decoder := json.NewDecoder(r.Body)
	decoder.DisallowUnknownFields()
	err := decoder.Decode(&user)
	if err != nil {
		panic(err)
	}
	if isUserExist(user.Login) {
		w.WriteHeader(http.StatusConflict)
		b, _ := json.Marshal(types.Error{"Пользователь с таким логином уже существует"})
		w.Write(b)
	} else {
		user.ID = primitive.NewObjectID()
		user.Password = private.HashPassword(user.Password)

		CreateUser(&user)
		expiration := time.Now().Add(7 * 24 * time.Hour)

		token := private.GetJwtField(user.ID.Hex(), user.Password)

		cookie := http.Cookie{Name: "token", Value: token, Expires: expiration, Path: "/"}
		http.SetCookie(w, &cookie)
		w.WriteHeader(http.StatusCreated)
	}
}

func getUserProfile(w http.ResponseWriter, r *http.Request) {
	clientUser := private.CheckJwtField(r)
	if clientUser != nil {
		log.Println("auth success: " + clientUser.Login)
		b, _ := json.Marshal(types.UserToClientUser(clientUser))
		w.WriteHeader(http.StatusOK)
		w.Write(b)
	} else {
		w.WriteHeader(http.StatusForbidden)
		log.Println("auth faild")
	}
}

func getUserReviews(w http.ResponseWriter, r *http.Request) {
	clientUser := private.CheckJwtField(r)
	if clientUser != nil {

		reviewsCollection := db.Client.Database("review").Collection("reviews")
		var reviews []types.ClientReview
		cur, err := reviewsCollection.Find(db.Ctx, bson.D{{"userId", clientUser.ID}})
		if err != nil {
			panic(err)
		}
		housesMapIds := map[string]string{}
		for cur.Next(context.TODO()) {

			// create a value into which the single document can be decoded
			var review types.ClientReview
			err := cur.Decode(&review)
			if err != nil {
				log.Fatal(err)
			}
			housesMapIds[review.HouseID] = review.HouseID
			reviews = append(reviews, review)
		}
		houseCollection := db.Client.Database("review").Collection("houses")
		for housesId := range housesMapIds {
			house := types.House{}
			houseCollection.FindOne(db.Ctx, bson.D{{"_id", housesId}}).Decode(&house)
			housesMapIds[housesId] = house.Description
		}
		for index := range reviews {
			reviews[index].Description = housesMapIds[reviews[index].HouseID]
		}
		b, _ := json.Marshal(reviews)

		w.WriteHeader(http.StatusOK)
		w.Write(b)
	} else {
		w.WriteHeader(http.StatusForbidden)
		log.Println("auth faild")
	}
}

// API END

func HandleUser() {
	http.HandleFunc("/api/user/create", create)
	http.HandleFunc("/api/user/login", login)
	http.HandleFunc("/api/user/reviews", getUserReviews)
	http.HandleFunc("/api/user", getUserProfile)
}

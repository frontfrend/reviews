package google

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
)

const AutosuggestUrl = "https://maps.googleapis.com/maps/api/place/autocomplete/json?types=geocode&key=AIzaSyCX7AiDF6kUbZFEP7CaPhavPdGIbLv78m4&language=ru&location=55.759191,37.619248"
const placeUrl = "https://maps.googleapis.com/maps/api/place/details/json?key=AIzaSyCX7AiDF6kUbZFEP7CaPhavPdGIbLv78m4&language=ru&fields=geometry"

func autosuggest(w http.ResponseWriter, r *http.Request) {
	query, err1 := r.URL.Query()["query"]
	if !err1 || query[0] == "" || len(query) == 0 {
		fmt.Println("Query is empty")
		w.Header().Set("Content-Type", "application/json")
		w.Write([]byte("{}"))
		return
	}
	finalURL := AutosuggestUrl + "&input=" + url.QueryEscape(query[0])
	asuggest, err := http.Get(finalURL)
	if err != nil {
		fmt.Println(err.Error())
	}
	body, _ := ioutil.ReadAll(asuggest.Body)
	fmt.Println(finalURL)

	w.Write(body)
}

type Location struct {
	Lat float64 `json:"lat"`
	Lng float64 `json:"lng"`
}

type Geometry struct {
	Location Location `json:"location"`
}

type Place struct {
	Geometry Geometry `json:"geometry"`
}

type PlaceResponse struct {
	Result Place `json:"result"`
}

func GetPlace(placeId string) *PlaceResponse {
	resp, err := http.Get(placeUrl + "&placeid=" + placeId)
	if err != nil {
		fmt.Println(err)
	}
	defer resp.Body.Close()

	place := PlaceResponse{}
	decoder := json.NewDecoder(resp.Body)
	decodeError := decoder.Decode(&place)
	if decodeError != nil {
		fmt.Println(decodeError)
	}
	fmt.Println(place.Result.Geometry.Location)
	return &place
}

func HandleGoogle() {
	http.HandleFunc("/api/addres/autosuggest", autosuggest)
}

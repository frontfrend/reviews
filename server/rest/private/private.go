package private

import (
	"fmt"
	"log"
	"net/http"

	"github.com/dgrijalva/jwt-go"
	"gitlab.com/frontfrend/reviews/server/db"
	types "gitlab.com/frontfrend/reviews/server/rest"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"golang.org/x/crypto/bcrypt"
)

const JwtName = "token"

var hmacSampleSecret = []byte("wefw234~fefwehweh4gfWFwg")

type CustomClaims struct {
	Id   string
	Hash string
}

type QQqq struct {
	Login string
}

func (c CustomClaims) Valid() error {
	return nil
}

func GetJwtField(id string, hash string) string {
	claims := CustomClaims{}
	claims.Id = id

	claims.Hash = hash
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)

	tokenString, err := token.SignedString(hmacSampleSecret)
	if err != nil {
		fmt.Println(err)
	}

	return tokenString
}

func createToken() {
}

func CheckJwtField(r *http.Request) *types.User {
	tokenString, err := r.Cookie(JwtName)
	if err != nil {
		fmt.Println(err)
		return nil
	}
	customClaims := CustomClaims{}
	token, err := jwt.ParseWithClaims(tokenString.Value, &customClaims, func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("Unexpected signing method: %v", token.Header["alg"])
		}

		return hmacSampleSecret, nil
	})

	if err != nil {
		fmt.Println(err)
		return nil
	}

	claims := token.Claims.(*CustomClaims)

	collection := db.Client.Database("review").Collection("users")
	oId, err := primitive.ObjectIDFromHex(claims.Id)

	if err != nil {
		fmt.Println(err)
		return nil
	}

	finedUser := types.User{}
	log.Println(claims.Hash)
	log.Println(oId)
	findError := collection.FindOne(db.Ctx, bson.D{{"_id", oId}, {"password", claims.Hash}}).Decode(&finedUser)

	if findError != nil {
		fmt.Println("Can't find user (CheckJwtField)", findError)

		return nil
	}
	return &finedUser
}

func HashPassword(password string) string {
	bytes, err := bcrypt.GenerateFromPassword([]byte(password), 14)
	if err != nil {
		fmt.Println(err)
	}
	return string(bytes)
}

func CheckPasswordHash(password, hash string) bool {
	err := bcrypt.CompareHashAndPassword([]byte(hash), []byte(password))
	return err == nil
}

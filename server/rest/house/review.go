package house

import (
	"context"
	"encoding/json"
	"fmt"
	"log"
	"net/http"

	"gitlab.com/frontfrend/reviews/server/db"
	types "gitlab.com/frontfrend/reviews/server/rest"
	"go.mongodb.org/mongo-driver/bson"
)

func GetHouse(clientReview *types.ClientReview) *types.House {
	houses := db.Client.Database("review").Collection("houses")

	filter := bson.D{{"placeId", clientReview.HouseID}}
	house := types.House{}
	houseDoesntExist := houses.FindOne(db.Ctx, filter).Decode(&house)
	if houseDoesntExist != nil {
		fmt.Println("Create house")
		house.Location = clientReview.Location
		house.Id = clientReview.HouseID
		house.Description = clientReview.Description
		houses.InsertOne(db.Ctx, house)
		return &house
	}
	return &house
}

func getHouses(w http.ResponseWriter, r *http.Request) {
	reviewsCollection := db.Client.Database("review").Collection("houses")
	var houses []types.MapHouse
	cur, err := reviewsCollection.Find(db.Ctx, bson.D{})
	if err != nil {
		panic(err)
	}
	for cur.Next(context.TODO()) {

		// create a value into which the single document can be decoded
		var house types.MapHouse
		err := cur.Decode(&house)
		if err != nil {
			log.Fatal(err)
		}

		houses = append(houses, house)
	}

	b, _ := json.Marshal(houses)
	w.WriteHeader(http.StatusOK)
	w.Write(b)
}

func houseById(id string) *types.House {
	houseCollection := db.Client.Database("review").Collection("houses")
	house := types.House{}
	houseCollection.FindOne(db.Ctx, bson.D{{"_id", id}}).Decode(&house)

	return &house
}

func getHouseReviews(w http.ResponseWriter, r *http.Request) {
	reviewsCollection := db.Client.Database("review").Collection("reviews")
	houseId := r.URL.Query().Get("houseId")
	house := houseById(houseId)
	var reviews []types.ClientReview
	cur, err := reviewsCollection.Find(db.Ctx, bson.D{{"houseId", houseId}})
	if err != nil {
		panic(err)
	}

	for cur.Next(context.TODO()) {
		var review types.ClientReview
		err := cur.Decode(&review)
		if err != nil {
			log.Fatal(err)
		}
		reviews = append(reviews, review)
	}
	for _, review := range reviews {
		review.Description = house.Description
	}
	b, _ := json.Marshal(reviews)
	w.WriteHeader(http.StatusOK)
	w.Write(b)
}

// HandleReview handle review's rest
func HandleHouses() {
	http.HandleFunc("/api/houses/houses", getHouses)
	http.HandleFunc("/api/houses/house", getHouseReviews)
}

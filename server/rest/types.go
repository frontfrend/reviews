package types

import (
	"time"

	"gitlab.com/frontfrend/reviews/server/rest/google"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

type Error struct {
	Message string `json:"message"`
}

type User struct {
	ID        primitive.ObjectID `bson:"_id,omitempty" json:"id"`
	CreatedAt time.Time          `json:"createdAt"`
	Login     string             `json:"login"`
	Password  string             `json:"password"`
}

type ClientUser struct {
	Login string `json:"login"`
}

type Term struct {
	Offset int    `json:"offset"`
	Value  string `json:"value"`
}

type Review struct {
	ID        primitive.ObjectID `bson:"_id,omitempty" json:"id"`
	UserID    primitive.ObjectID `bson:"userId" json:"userId"`
	HouseID   string             `bson:"houseId" json:"houseId"`
	Apartment int                `json:"apartment"`
	Rate      int                `json:"rate"`
	Review    string             `json:"review"`
}

type House struct {
	Id          string          `bson:"_id" json:"id"`
	Location    google.Location `json:"location"`
	Description string          `json:"description"`
}

type MapHouse struct {
	Id       string          `bson:"_id" json:"id"`
	Location google.Location `json:"location"`
}

type HouseInfo struct {
	ClientReviews []ClientReview `json:"reviews"`
	Description   string         `json:"description"`
}

type ClientReview struct {
	ID          primitive.ObjectID `bson:"_id,omitempty" json:"id"`
	Description string             `json:"description"`
	Apartment   int                `json:"apartment"`
	Rate        int                `json:"rate"`
	Review      string             `json:"review"`
	HouseID     string             `bson:"houseId" json:"houseId"`
	Location    google.Location    `json:"location"`
}

type EditableReview struct {
	ID        primitive.ObjectID `bson:"_id,omitempty" json:"id"`
	Apartment int                `json:"apartment"`
	Rate      int                `json:"rate"`
	Review    string             `json:"review"`
}

type DeleteReview struct {
	ID      primitive.ObjectID `bson:"_id,omitempty" json:"id"`
	HouseID string             `bson:"houseId" json:"houseId"`
}

func UserToClientUser(user *User) *ClientUser {
	cUser := ClientUser{Login: user.Login}
	return &cUser
}

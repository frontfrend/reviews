package db

import (
	"context"
	"fmt"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

const database = "review"

var Client *mongo.Client
var Ctx, Cancel = context.WithCancel(context.Background())

func init() {

	client, err := mongo.Connect(Ctx, options.Client().ApplyURI("mongodb://admin:asdzxcsx@5.253.62.72:27017/"+database+"?authSource=admin&retryWrites=true&w=majority"))

	if err != nil {
		fmt.Println(err.Error())
	} else {
		Client = client
		fmt.Println("MongoDB Client: ✅😍")

		Client.Database("review").Collection("reviews").FindOne(Ctx, bson.D{})
	}
}

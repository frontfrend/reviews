module gitlab.com/frontfrend/reviews

go 1.12

require (
	github.com/gbrlsnchs/jwt/v3 v3.0.0-beta.0 // indirect
	golang.org/x/crypto v0.0.0-20190513172903-22d7a77e9e5f // indirect
	golang.org/x/net v0.0.0-20190514140710-3ec191127204 // indirect
	golang.org/x/sys v0.0.0-20190516110030-61b9204099cb // indirect
	golang.org/x/text v0.3.2 // indirect
	golang.org/x/tools v0.0.0-20190517183331-d88f79806bbd // indirect
	gopkg.in/square/go-jose.v2 v2.3.1 // indirect
)
